Source: bashtop
Maintainer: Dylan Aïssi <daissi@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/bashtop
Vcs-Git: https://salsa.debian.org/debian/bashtop.git
Homepage: https://github.com/aristocratos/bashtop
Rules-Requires-Root: no

Package: bashtop
Architecture: all
Depends: bash (>= 5.0),
         ${misc:Depends},
         ${shlibs:Depends},
         gawk,
         procps
Recommends: lm-sensors,
            sysstat,
            python3-psutil (>= 5.7.0),
            curl
Multi-Arch: foreign
Description: Resource monitor that shows usage and stats
 Resource monitor that shows usage and stats for processor,
 memory, disks, network and processes. bashtop includes:
  - Easy to use, with a game inspired menu system.
  - Fast and responsive UI with UP, DOWN keys process selection.
  - Function for showing detailed stats for selected process.
  - Ability to filter processes.
  - Easy switching between sorting options.
  - Send SIGTERM, SIGKILL, SIGINT to selected process.
  - UI menu for changing all config file options.
  - Auto scaling graph for network usage.
  - Shows current read and write speeds for disks.
  - Multiple data collection methods which can be switched if running on Linux.
